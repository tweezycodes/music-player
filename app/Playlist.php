<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Playlist extends Model
{
    protected $table = "playlist";
    public $timestamps = true;

    protected $fillable = [
		'title'
	];
}
