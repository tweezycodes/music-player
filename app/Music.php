<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Music extends Model
{
    protected $table = "music";
    public $timestamps = true;

    protected $fillable = [
		'title','artist','file'
	];
}
