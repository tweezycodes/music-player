<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Playlist;

class PlaylistController extends Controller
{
    public function storePlaylist(Request $request) {

        $id = Auth::id();
        $playlist = new Playlist();
        $playlist->title = $request->title;
        $playlist->user = $id;
        $playlist->save();
        return $playlist;
              
    }

    public function getMyPlaylist(Request $request) {
        
		$id = Auth::id();
        
        $playlist = DB::table('playlist')->where('user', $id)->get();
        return $playlist;
    }
}
