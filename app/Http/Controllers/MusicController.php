<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Music;
class MusicController extends Controller
{
    public function storeMusic(Request $request) {
    	


        $fileName = time().'.'.$request->file->getClientOriginalExtension();
        $request->file->move(public_path('upload'), $fileName);

        $id = Auth::id();
        $music = new Music();
        $music->title = $request->title;
        $music->artist = $request->artist;
        $music->file = $fileName;
        $music->user = $id;
        $music->save();
        return $music;
              
    }

    public function getMyMusic(Request $request) {
        
		$id = Auth::id();
        

        $music = DB::table('music')->where('user', $id)->get();
        return $music;
    }

    public function deleteMusic(Request $request){
        $music = Music::find($request->id)->delete();
    }
}
