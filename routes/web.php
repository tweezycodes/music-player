<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


Route::post('/storeMusic','MusicController@storeMusic');
Route::get('/getMyMusic', 'MusicController@getMyMusic');
Route::post('/deleteMusic/{id}', 'MusicController@deleteMusic');
Route::post('/storePlaylist','PlaylistController@storePlaylist');
Route::get('/getMyPlaylist', 'PlaylistController@getMyPlaylist');
// Route::post('/editCars/{id}', 'CarsController@editCar');
