@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="text-center">
                        <h3 id="music-details-container"></h3>
                        <br>
                        <button @click="backMusic()" class="player-btn"  disabled><i class="fa fa-backward "></i></button>
                        <button @click="playMusicNow()" disabled  class="d-none player-btn"  id="play-music-btn"><i class="fa fa-play"></i></button>
                        <button @click="pauseMusic()" disabled class="player-btn" id="pause-music-btn" ><i class="fa fa-pause"></i></button>
                        <button @click="stopMusic()" disabled class="player-btn"  ><i class="fa fa-stop"></i></button>
                        <button @click="nextMusic()" disabled class="player-btn" ><i class="fa fa-forward"></i></button>
                    </div>
                   
                    <div id="app">
                        <div class="card-header tab-card-header">
                            <ul class="nav nav-tabs card-header-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link" id="one-tab" data-toggle="tab" href="#one" role="tab" aria-controls="One" aria-selected="true">My Music</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="two-tab" data-toggle="tab" href="#two" role="tab" aria-controls="Two" aria-selected="false">My Playlists</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="three-tab" data-toggle="tab" href="#three" role="tab" aria-controls="Three" aria-selected="false">Account</a>
                                </li>
                          </ul>
                        </div>

                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active p-3" id="one" role="tabpanel" aria-labelledby="one-tab">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#upload-music-modal">Upload Music</button>
                                <table class="table table-striped" id="table">
                                    <thead>
                                        <tr>
                                        <th scope="col">Title</th>
                                        <th scope="col">Artist</th>
                                        <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for ="m in music">
                                        <td>@{{m.title}}</td>
                                        <td>@{{m.artist}}</td>
                                        <td> <button @click="playMusic(m)"  class="btn btn-primary btn-sm" class="btn btn-info btn-lg" data-toggle="modal" ><i class="fa fa-play"></i></button>
                                            <button @click="deleteMusic(m)"  class="btn btn-danger btn-sm" class="btn btn-info btn-lg" data-toggle="modal" ><i class="fa fa-trash"></i></button>
                                        </td>
                                    
                                        </tr>
                                    </tbody>
                                </table>
                          </div>
                          <div class="tab-pane fade p-3" id="two" role="tabpanel" aria-labelledby="two-tab">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#create-playlist-modal">Create New Playlist</button>
                                <table class="table table-striped" id="playlist-table">
                                    <thead>
                                        <tr>
                                        <th scope="col">Title</th>
                                        <th scope="col"></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr v-for ="p in playlist">
                                            <td>@{{p.title}}</td>
                                            <td> 
                                                <button @click="viewPlaylist(p)"  class="btn btn-primary btn-sm" class="btn btn-info btn-lg"  ><i class="fa fa-eye"></i></button>
                                                <button @click="deletePlaylist(p)"  class="btn btn-danger btn-sm" class="btn btn-info btn-lg" ><i class="fa fa-trash"></i></button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                          </div>
                          <div class="tab-pane fade p-3" id="three" role="tabpanel" aria-labelledby="three-tab">
                          </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div id="upload-music-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridModalLabel">Upload Music</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data" method="POST" >
                     <div class="alert alert-danger d-none" role="alert" id="upload-file-alert">
                        All fields are required!
                    </div>
                    <div class="form-group">
                        <label>MP3 File</label>
                        <input type="file" id="file" ref="file" v-on:change="handleFileUpload()"/>
                    </div>
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="title" required placeholder="Title" name="title" v-model="newMusic.title">
                    </div>
                                                            
                    <div class="form-group">
                        <label for="model">Artist</label>
                        <input type="text" class="form-control" id="artist" required placeholder="Artist" name="artist" v-model="newMusic.artist">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" @click.prevent="createMusic()">
                    Add Music
                </button>
            </div>
        </div>
    </div>
</div>


<div id="create-playlist-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="gridModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="gridModalLabel">Create Playlist</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <form enctype="multipart/form-data" method="POST" >
                     <div class="alert alert-danger d-none" role="alert" id="create-playlist-alert">
                        All fields are required!
                    </div>
                   
                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" class="form-control" id="name" required placeholder="Name" name="name" v-model="newPlaylist.name">
                    </div>
                                                            
                 
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button class="btn btn-primary" @click.prevent="createPlaylist()">
                    Add Playlist
                </button>
            </div>
        </div>
    </div>
</div>
@endsection
