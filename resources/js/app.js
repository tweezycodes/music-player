require('./bootstrap');

window.Vue = require('vue');

Vue.component('example-component', require('./components/ExampleComponent.vue').default);
var currentMusic;
var currentFile;
var currentMusicTime = 0;
var pausedFile;
var currentMusicID;
var allMusic;
var lastMusicID
const app = new Vue({
    el: '#app',

 	mounted: function mounted(){
        this.getMusic();
	    this.getPlaylist();
	},

    data: {
        newMusic: {'title': '', 'artist': '', 'file':null},
        music: [],
        newPlaylist: {'title': ''},
        playlist:[],

        handleFileUpload(){
		    this.file = this.$refs.file.files[0];
		},

		createPlaylist: function createPlaylist() {
			let formData = new FormData();
            var _this = this;
            var input = this;

            formData.append('title', this.newMusic.title);
            if(input['title'] == ''  ) {
            	$('#upload-file-alert').removeClass('d-none')
            } else {
            	$('#upload-file-alert').addClass('d-none')
            	
            	axios.post( '/storePlaylist',
	                formData,
	                {
	                
	              }
	            )
		        .then(function(){
		        	$('#create-playlist-modal').modal('hide')
	                _this.newPlaylist = { 'title': ''}
	                _this.getPlaylist();    
		           console.log('success');
		        })
		        .catch(function(){
		           console.log('failed');
		        });
            }
		},

        getPlaylist: function getPlaylist(){
            var _this = this;
            axios.get('/getMyPlaylist').then(function(response){
                _this.playlist = response.data;
                // allMusic = response.data
                // last_element = allMusic[allMusic.length - 1];
                 // lastMusicID = last_element.id
            }).catch(error=>{
                console.log("Get All: "+error);
            });
        },

        createMusic: function createMusic() {
         	
            let formData = new FormData();
            var _this = this;
            var input = this;

            formData.append('title', this.newMusic.title);
            formData.append('artist', this.newMusic.artist);
            formData.append('file', this.file);
            if(input['title'] == '' || input['artist'] == '' || !this.file ) {
            	$('#upload-file-alert').removeClass('d-none')
            } else {
            	$('#upload-file-alert').addClass('d-none')
            	
            	axios.post( '/storeMusic',
	                formData,
	                {
	                headers: {
	                    'Accept': 'application/json',
	                    'Content-Type': 'multipart/form-data'
	                }
	              }
	            )
		        .then(function(){
		        	$('#upload-music-modal').modal('hide')
	                _this.newMusic = {'artist': '', 'title': ''}
	                _this.getMusic();    
		           console.log('success');
		        })
		        .catch(function(){
		           console.log('failed');
		        });
            }
	          
        },

        deleteMusic: function deleteMusic(m) {
		  var _this = this;
		  axios.post('/deleteMusic/' + m.id).then(function(response){
		   _this.getMusic();
		      }).catch(error=>{
		            console.log("Delete music: "+error);
		            });
        },

        getMusic: function getMusic(){
            var _this = this;
            axios.get('/getMyMusic').then(function(response){
                _this.music = response.data;
                allMusic = response.data
                last_element = allMusic[allMusic.length - 1];
               	 lastMusicID = last_element.id
            }).catch(error=>{
                console.log("Get All: "+error);
            });
        },

        playMusic: function playMusic(m) {
        	file = m.file
			$('#music-details-container').html(m.title+' - '+m.artist)
			$('.player-btn').removeAttr('disabled')
        	if (currentFile || pausedFile) {
        		if (currentFile == file) {
        			console.log('pause')
        				currentMusic.pause();
	    	 			currentFile = null;
	    	 			pausedFile = file;

        		} else if (pausedFile == file) {
        			currentFile = file
        			currentMusic.currentTime = currentMusicTime;
        			currentMusic.play();
    	 			currentMusicID = m.id
        			console.log('resume ')
        		} else {
        			console.log('new')
        			currentMusic.pause();
        	 		audio = new Audio('/upload/'+file);
        	 		audio.play() 
    	 			currentFile = file;
        	 		currentMusic = audio;
    	 			currentMusicID = m.id
        		}
        	} else {
        		console.log('first')
    	 		audio = new Audio('/upload/'+file);
    	 		
    	 		audio.play() 
    	 		currentMusic = audio;
    	 		currentFile = file;
    	 		currentMusicID = m.id
        	}
        	 	
        },

        stopMusic:function stopMusic() {
        	currentMusic.pause();
        	currentFile = null;
			$('#music-details-container').html('')
			$('.player-btn').attr('disabled','disabled')
 			$('#play-music-btn').addClass('d-none')
 			$('#pause-music-btn').removeClass('d-none')
        },
        pauseMusic:function pauseMusic() {
        	currentMusic.pause();

 			$('#play-music-btn').removeClass('d-none')
 			$('#pause-music-btn').addClass('d-none')
        },
        playMusicNow:function playMusicNow() {
        	currentMusic.play();

 			$('#play-music-btn').addClass('d-none')
 			$('#pause-music-btn').removeClass('d-none')
        },

        nextMusic:function nextMusic() {
   
        	currentMusic.pause();

        	console.log(allMusic)
        	next = currentMusicID;
        	next++;
        	for (i in allMusic) {
        		if (allMusic[i].id == next) {
        			newMusic = allMusic[i];
        			break;

        		}
                next++
        	}

        	if (next > lastMusicID) {
        		currentMusic.pause();
	        	currentFile = null;
				$('#music-details-container').html('')
				$('.player-btn').attr('disabled','disabled')
	 			$('#play-music-btn').addClass('d-none')
	 			$('#pause-music-btn').removeClass('d-none')

        	} else {
        		audio = new Audio('/upload/'+newMusic.file);
    	 		audio.play() 
	 			currentFile = newMusic.file;
    	 		currentMusic = audio;
	 			currentMusicID = newMusic.id
				$('#music-details-container').html(newMusic.title+' - '+newMusic.artist)
        	}


        },

        backMusic:function backMusic() {
   
        	currentMusic.pause();
        	next = currentMusicID;
            next--;
            console.log(currentMusicID+' '+next);

        	for (i in allMusic) {
        		if (allMusic[i].id == next) {
        			// newMusic = allMusic[i];
        			break;
        		}
        	}


    //     	if (next == 0) {
    //     		currentMusic.pause();
	   //      	currentFile = null;
				// $('#music-details-container').html('')
				// $('.player-btn').attr('disabled','disabled')
	 		// 	$('#play-music-btn').addClass('d-none')
	 		// 	$('#pause-music-btn').removeClass('d-none')

    //     	} else {
    //     		audio = new Audio('/upload/'+newMusic.file);
    // 	 		audio.play() 
	 		// 	currentFile = newMusic.file;
    // 	 		currentMusic = audio;
	 		// 	currentMusicID = newMusic.id
				// $('#music-details-container').html(newMusic.title+' - '+newMusic.artist)
    //     	}

        },

         
     }


});
